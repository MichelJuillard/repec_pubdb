# REPEC_PUBDB: an application to exploit REPEC data inside an organization

PUBDB manages an internal copy of REPEC records of publications for the authors
affiliated with a given organization.

## Prerequisites
### Access to REPEC API

PUBDB uses REPEC API in order to download information. You need first to
obtain an access code from REPEC. Follow instructions listed at https://ideas.repec.org/api.html

### Software
 - PUBDB is written in **Python 3**. Additional packages will be downloaded
   as part of the installation process (see below)
 - PUBDB is meant to be run on a Linux server able to send mail (for
   user registration)
 - The database is stored in **MongoDB** that must be already installed on the
   server
 - Web access is managed by **nginx** and **uwsgi** (on Debian, install packages
   **nginx** and **uwsgi**)
- The code distribution is based on **git** (on Debian, install package **git**)

## Installation
### Download code

Choose the installation directory and download the code:
```
	git clone https://gitlab.com/MichelJuillard/repec_pubdb.git
```
### Configuration
Configuration is separated in two files:
 - **repec_pubdb/pubdb/default_config.py** contains the configuration
 by default
 - **repec_pubdb/pubdb/local_config.py** contains  the configuration
 for a particular configuration. YOU MUST CREATE yourself
 **local_config.p** by editing the template provided in
 **repec_pubdb/pubdb/local_config.template**
 
### Local configuration variables
 - **REPEC_API_CODE**: the code necessary to connect to REPEC API and
 that has been given to you by REPEC administration,
 - **REPEC_INST_HANDLE**: the REPEC handle of your organization,
 - **DOMAIN**: the complete Internet domain of your server. Note that
   **DOMAIN** is used to define the **sender** used in mail messages sent
 by the server.
 
You can also used **local_config.py** to modify the default of the
 other configuration variables.
  
### Create and activate a virtual environment
To create a virtual environment:
```
	cd repec_pubdb
	python3 -m venv venv
```
This needs to be done only once.

To activate this environment:
```
	source venv/bin/activate
```
This needs to be done everytime that you are opening a new shell in
order to work with Flask command line instructions.

### Install the code in the virtual environment
```
	pip install ./repec_pubdb
```
If you modify **local_config.py** or other parts of the code after
initial installation, you must upgrade your installed package with
```
	pip install --upgrade ./repec_pubdb
```

### UWSGI configuration
You need aministrator rights on your server to do this
configuration. Add a file /etc/uwsgi/apps-available/pubdb.ini
containing
```
[uwsgi]
chdir = PATH_TO_REPEC_PUBDB

app = pubdb
module = %(app)
virtualenv = PATH_TO_REPEC_PUBDB/venv

socket = /tmp/uwsgi_pubdb.sock

chmod-socket = 666

callable = app

logto /var/log/uwsgi/%n.log
```

where you must replace PATH_TO_REPEC_PUBDB by the absolute directory
to **repec_pubdb**. Then, add a symbolic link to pubdb.ini in the
/etc/uwsgi/apps-enabled/ directory:

```
sudo ln -s /etc/uwsgi/apps-available/pubdini /etc/uwsgi/apps-enabled/pubdb.ini
```

### NGINX configuration
You need administrator rights on your server to do this configuration. In the
**nginx** configuration file, you need to have the following fragment:
```
       location / {
       		include uwsgi_params;
		uwsgi_pass unix:/tmp/uwsgi_pubdb.sock;
       }
```
The Flask server needs to be at the root. If you have several web
sites on the same server, use different domain names or different
ports (see **nginx** documentation).

### Setting up evironment variable
In order to use Flask in shell mode for command line instructions, you need to configure the
environment variable **FLASK_APP**:
```
	export FLASK_APP=pubdb 
```
This needs to be done everytime that you are opening a new shell or
put in your shell initialization file.
	
	
## Data management
The data are aquired from REPEC and updated via command line
instructions:
- update papers:

```
	flask update_papers_repec
```

## User management
Users must register using the web site interface.

## Features
From the web interface, it is possible to search papers in the
database and to generate reports:
- number of articles per year
- numer of articles per JEL code and per year

## Limitation
- The REPEC database collects all papers published in the past by authors
  currently associated with a given institution. This doesn't mean
  that these papers have been published while the author was working
  for that institution. It is neccessary to add a manual flag for the
  papers that can be claimed by a given institution. This will be
  added in a future version.

## Acknowledgments
I thank Jeanne Houza-Oufinon and Mohamed Shit (Ecole Centrale de Marseille)
who have worked on a previous version of this app during their
internship as well as Thomas Krichler and Christian Zimmermann, from
REPEC, for help and discussions. 
