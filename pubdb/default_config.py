# Flask
WTF_CSRF_ENABLED = True
SECRET_KEY = b'1ccd512f8c3493797a657c32db38e7d51ed74f14fa7580'
#Database
MONGODB_DB = 'pubdb'
# Flask Security
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
SECURITY_REGISTERABLE = True
SECURITY_CONFIRMABLE = True
SECURITY_RECOVERABLE = True
SECURITY_TRACKABLE = True
SECURITY_CHANGEABLE = True
SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_PASSWORD_SALT = '11'
SECURITY_POST_LOGOUT_VIEW = '/login'
# REPEC
REPEC_API_URL = "https://api.repec.org/call.cgi"
REPEC_LAST_RANKINGS_URL = "https://ideas.repec.org/top"
REPEC_RANKINGS_URL_ROOT = "https://ideas.repec.org/top/old/"
FIRST_RANKING_DATE = "0702"


