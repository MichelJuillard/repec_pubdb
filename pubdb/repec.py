import requests
from bs4 import BeautifulSoup
import re
import datetime
from json import loads

def get_inst_authors(config):
    payload = {'code': config["REPEC_API_CODE"],
               'getinstauthors': config["REPEC_INST_HANDLE"]}
    r = requests.get(config["REPEC_API_URL"], params=payload)
    r.raise_for_status()
    return r.json()

def get_author_record_full(author_shortid, config):
    payload = {'code': config["REPEC_API_CODE"],
               'getauthorrecordfull': author_shortid}
    r = requests.get(config["REPEC_API_URL"], params=payload)
    r.raise_for_status()
    return r.json()

def get_jel_codes(item_handle, config):
    payload = {'code': config["REPEC_API_CODE"],
               'getjelforitem': item_handle}
    r = requests.get(config["REPEC_API_URL"], params=payload)
    r.raise_for_status()
    return r.json()

def get_ref(item_handle, config):
    payload = {'code': config["REPEC_API_CODE"],
               'getref': item_handle}
    r = requests.get(config["REPEC_API_URL"], params=payload)
    r.raise_for_status()
    return r.json()
    
def decrease(year,month):
    if month > 1:
        month -= 1
    else:
        year -= 1
        month = 12
    return (year,month)

def get_last_rankings():
    today = datetime.date.today()
    year = today.year
    month = today.month
    rankings = None
    try:
        rankings = get_rankings_by_year_month(str(year),str(month))
    except requests.exceptions.HTTPError:
        error = True
        max_error = 0
        while error and max_error < 10:
            (year, month) = decrease(year, month)
            try: 
                rankings = get_rankings_by_year_month(str(year)[-2:],"{:02d}".format(month))
                error = False
            except requests.exceptions.HTTPError:
                max_error += 1
    return rankings

def get_rankings_by_year_month(year, month, config):
    print(config["REPEC_RANKINGS_URL_ROOT"] + year + month)
    return get_rankings(config["REPEC_RANKINGS_URL_ROOT"] + year + month)

def get_rankings(link):
    fields = get_ranking_fields(link)
    rankings =  []
    for f in fields:
        try:
            print(f["url"])
            r = get_institution_rankings_by_field(f["url"],f["id"],f["name"])
        except:
            print("Error: can't download {}".format(f["url"]))
        else:
            rankings += r
    return rankings

def get_ranking_fields(link):
    r = requests.get(link)
    r.raise_for_status()

    local_page = r.content

    soup = BeautifulSoup(local_page, 'html.parser')

    h3s = soup.find_all('div', id="author-summary")[0].find_all('h3')
    for h in h3s:
        if h.string == 'Top institutions by field':
            top_inst_h3 = h
            break

    Instit_by_field = h.find_next("p")

    List_of_Fields = []
    for x in range(len(Instit_by_field.contents))[1:-1]:
        if x % 2 == 0:
            continue
        if not re.search(r"\b\w\w\w\b", Instit_by_field.contents[x].text):
            break

        field = {}
        a = Instit_by_field.contents[x]
        field["url"] = a['href']
        field["id"] = a.text

        f_name = str(Instit_by_field.contents[x + 1]).replace(',', '').replace('.', '').strip()
        field["name"] = f_name.replace('\\n', '', 3)

        List_of_Fields.append(field)

    return List_of_Fields

def get_institution_rankings_by_field(link, field_id, field_name):
    r = requests.get(link)
    r.raise_for_status()

    local_page = r.content

    soup = BeautifulSoup(local_page, 'html.parser')

    List_Ranks = []

    Field_div = soup.find('div', id="content-block").table

    Field_div_tbody_tr = Field_div.find_all("tr")
    Field_div_tbody_tr.remove(Field_div_tbody_tr[0])

    rank = ""

    for instit in Field_div_tbody_tr:

        mini_dict = {}
        mini_dict["field_id"] = field_id
        mini_dict["field_name"] = field_name
        mini_dict["repec_handle"] = instit.td.find('a')["name"]

        if instit.td.get_text() != '---':
            rank = instit.td.get_text()

        mini_dict["rank"] = rank

        instit_td_all = instit.find_all('td')
        instit_td = instit.find_all('td')[1]
        instit_a = instit_td.find('a', href=True)

        mini_dict["name"] = instit_a.text
        instit_p = instit_td.find('p')
        mini_dict["location"] = instit_p.text

        list = [x.text for x in instit.find_all('td')[2:]]

        try :
            mini_dict["score"]=list[0]
            mini_dict["authors_number"] = list[1]
            mini_dict["author_shares"] = list[2]

        except IndexError as err:
            print("Could not get entire rankings in following url : ", link)
            print("Raises following error : ", err)


        List_Ranks.append(mini_dict)


    return List_Ranks
