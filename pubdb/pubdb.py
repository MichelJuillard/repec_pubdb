from bson.objectid import ObjectId
import click
from flask import render_template, flash, redirect, Flask, request, url_for
from flask_mongoengine import MongoEngine
from flask_security import Security, MongoEngineUserDatastore, UserMixin, RoleMixin, login_required, roles_required, current_user
from flask_security.utils import encrypt_password
from flask_mail import Mail, Message
from mongoengine.queryset.visitor import Q


import sys, os, subprocess
from operator import itemgetter
from copy import copy
from re import match, compile
from datetime import datetime
from getpass import getpass

from pubdb.repec import get_inst_authors
from pubdb.publications import update_repec
from pubdb.forms import SearchPaperForm, JelCodesForm
from pubdb.models import Paper, Article, WorkingPaper
from pubdb.reports import number_of_articles_per_year, count_jel_codes, jel_classification

app = Flask(__name__)
app.config.from_object('pubdb.default_config')
app.config.from_object('pubdb.local_config')

mail = Mail(app)
# Create database connection object
db = MongoEngine(app)

class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)

class User(db.Document, UserMixin):
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])
    last_login_at = db.DateTimeField()
    current_login_at = db.DateTimeField()
    last_login_ip = db.StringField(max_length=255)
    current_login_ip = db.StringField(max_length=255)
    login_count = db.IntField()

# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)

@app.route('/', methods=['GET', 'POST'])
@login_required
def home():
    return render_template("home.html",
                           title='Home',
                           collections = ['Authors', 'Journals', 'Papers'])


@app.route("/search_papers", methods=['GET', 'POST'])
@login_required
def search_papers():
    form = SearchPaperForm(request.form)

    if request.method == 'GET':
        return render_template('search_papers.html',
                               title="SearchPapers",
                               form=form,
                               results=[])
    else:
        results = query_papers(form)
        return render_template("search_papers.html",
                               form = form,
                               results = results,
                               a_keys = [('author','Authors'),
                                         ('year','Year'),
                                         ('title','Title'),
                                         ('journal_name','Journal'),
                                         ('volume','Volume'),
                                         ('issue','Issue'),
                                         ('pages','Pages'),
                                         ('jel_codes','JEL codes')],
                               w_keys = [('author','Authors'),
                                         ('year','Year'),
                                         ('title','Title'),
                                         ('wpseries_name','Series'),
                                         ('number','Number'),
                                         ('jel_codes','JEL codes')])

@app.route("/report_articles_per_year")
@login_required
def report_articles_per_year():
    results = number_of_articles_per_year()
    return render_template("report_articles_per_year.html",results=results)

@app.route("/report_articles_by_jel_codes", methods=['GET', 'POST'])
@login_required
def report_articles_by_jel_code():
    if request.method == 'GET':
        form = JelCodesForm()
        return render_template("report_articles_by_jel_codes.html", form=form, table=[])
    else:
        papers = Article.objects
        f = request.form
        if "weighted" in request.form:
            weighted = True
        else:
            weighted = False
        year1 = int(f["year1"])
        year2 = int(f["year2"])
        jel_counts = count_jel_codes(papers, year1, year2, int(f["level"]))
        years = [str(y) for y in range(year1,year2+1)]
        nyears = year2 - year1 + 2
        table = []
        for code, values in jel_counts.items():
            row = [code, jel_classification[code]] + [" "]*nyears
            total = 0
            for y, count in values.items():
                row[int(y) - year1 + 2] = "{0:.1f}".format(count)
                total += count
            row[-1] = "{0:.1f}".format(total)
            table.append(row)
        table = [['Code','Category'] + years + ["Total"]] + sorted(table, key = itemgetter(0)) 
        return render_template("report_articles_by_jel_codes.html", form = JelCodesForm(f), table = table)
    
    

@app.cli.command()
def update_papers_repec():
    t1 = datetime.now()
    result = update_repec(app.config)
    tdelta = datetime.now() - t1
    if result:
        print("ERROR: {}".format(result))
    else:
        print("Elapsed time: {} seconds".format(tdelta))


    
def query_papers(form):
    query = Q()
    for f in form:
        if f.id == "csrf_token":
            continue
        data = f.data
        if data:
            if f.type == "SelectField":
                name = f.id
                query &= Q(**{name: data})
            else:
                for token in data.lower().split():
                    name = f.id + "__icontains"
                    query &= Q(**{name: token})
    return {'Article': Article.objects(query).order_by("-year"),
            'WorkingPaper': WorkingPaper.objects(query).order_by("-year")}

            
