import unittest
from mongoengine import connect
import httpretty
import logging
from pubdb.tests.httpretty_config import config_httpretty

class BaseTestCase(unittest.TestCase):

    def setUp(self):
        super().setUp()
        config_httpretty()
        logging.basicConfig(handlers=[logging.FileHandler("pubdb_test.log", 'w', 'utf-8')],
                            level=logging.DEBUG)


    def tearDown(self):
        httpretty.disable()
        httpretty.reset()

class BaseTestDBCase(unittest.TestCase):
    
    def setUp(self):
        super().setUp()


        # use/create new test database
        connect("pubdb_test",host="mongomock://localhost")
        # call httpretty after calling mongo client
        config_httpretty()
        logging.basicConfig(handlers=[logging.FileHandler("pubdb_test.log", 'w', 'utf-8')],
                            level=logging.DEBUG)

    def tearDown(self):
        httpretty.disable()
        #        self.test_backend.mongo_client.close()
