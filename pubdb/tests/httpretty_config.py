import requests
import httpretty
import unittest
import os
from pubdb import test_files
TEST_FILES_DIR = os.path.abspath(os.path.dirname(test_files.__file__))

def config_httpretty():

    path = '../'

    locals = {"https://edirc.repec.org/data/bdfgvfr.html": TEST_FILES_DIR + "/bdfgvfr.html",
              "https://ideas.repec.org/top/old/1608": TEST_FILES_DIR + "/Rankings_1608.html",
              "https://ideas.repec.org/top/old/1608/top.afr.html": TEST_FILES_DIR + "/Rankings_afr_1607.html",
              "https://ideas.repec.org/top/old/1608/top.acc.html": TEST_FILES_DIR + "/Rankings_acc_1607.html",
    }
    
    httpretty.enable()
    for link in locals.keys():
        with open(locals[link], encoding="utf-8") as f:
            local_page = f.read()
        httpretty.register_uri(httpretty.GET,
                               link,
                               body=local_page)
    httpretty.register_uri(httpretty.GET,
                           "https://api.repec.org/call.cgi",
                           body = dummy_call_cgi)


def end_httpretty_config():
    httpretty.disable()
    httpretty.reset()


def dummy_call_cgi(request, uri, headers):
    if request.querystring == {'code': ['XXXXXXXX'], 'getinstauthors': ['RePEc:edi:bdfgvfr']}:
        with open(TEST_FILES_DIR + "/bdf.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)
    if request.querystring == {'code': ['XXXXXXXX'], 'getauthorrecordfull': ['pju1']}:
        with open(TEST_FILES_DIR + "/pju1.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)
    if request.querystring == {'code': ['XXXXXXXX'], 'getjelforitem': ['RePEc:cpm:dynare:001']}:
        with open(TEST_FILES_DIR + "/jel_code.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)
    if "getjelforitem" in request.querystring:
        with open(TEST_FILES_DIR + "/jel_code_error.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)
    if request.querystring == {'code': ['XXXXXXXX'], 'getref': ['RePEc:eee:ecmode:v:24:y:2007:i:3:p:481-505']}:
        with open(TEST_FILES_DIR + "/paper.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)

