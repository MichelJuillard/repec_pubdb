from mongoengine import Document, EmbeddedDocument, FloatField, IntField, StringField, EmbeddedDocumentField, ListField, DateTimeField, BooleanField
from datetime import datetime

class Rankings(Document):
    year = IntField()
    month = IntField()
    field_id = StringField()
    field_name = StringField()
    institution_handle = StringField()
    institution_name = StringField()
    institution_location = StringField()
    score = FloatField()
    authors_number = IntField()
    author_shares = FloatField()
    
class Institution(EmbeddedDocument):
    name = StringField()
    share = FloatField()
    handle = StringField()

class Series(EmbeddedDocument):
    name = StringField()
    provider = StringField()
    handle = StringField()

class Author(Document):
    shortid = StringField(primary_key=True)
    name = StringField()
    email = StringField()
    url = StringField()
    affiliation = ListField(EmbeddedDocumentField(Institution))
    serieseditor = ListField(EmbeddedDocumentField(Series))
    paper_handles = ListField(StringField())
        
class Paper(Document):
    title = StringField()
    author = StringField()
    author_handles = ListField(StringField())
    link = StringField()
    doi = StringField()
    handle = StringField(primary_key=True)
    seriestype = StringField()
    year = StringField()
    jel_codes = ListField(StringField())
    modified_by = StringField()
    modification_date = DateTimeField()
    validated = BooleanField(default=False)
    forthcoming = BooleanField(default=False)
    
    meta = {'allow_inheritance': True}
    
class WorkingPaper(Paper):
    wpseries_handle = StringField()
    wpseries_name = StringField()
    number = StringField()

class Article(Paper):
    journal_name = StringField()
    journal_handle = StringField()
    volume = IntField()
    issue = StringField()
    pages = StringField()
    month = StringField()
    
