from setuptools import setup, find_packages

setup(
    name='pubdb',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'wheel',
        'flask',
        'flask_mongoengine',
        'flask_security',
        'bcrypt',
        'flask_mail',
        'requests',
        'bs4',
        'sure',
        'httpretty',
        'nose',
        'lxml'
    ],
)
